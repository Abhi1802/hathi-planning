----------------------------------------------------------------------

## Zot Protocol

The Zot protocol is the latest iteration in a long series of iterations
over a decentralized social network protocol, originally made for red
matrix; Zot is an improved version created for HubZilla.  
Lately an attempt is being made with Zot6 to simplify the protocol, 
probably in order to encourage adoption.

----------------------------------------------------------------------

### Adoption

HubZilla network, created by the same developer.

----------------------------------------------------------------------

### User base

The HubZilla users seem to be mainly discussing HubZilla.  
There are other channels/groups though, and some of them are presumably
active.  
I (UW) suspect that the whole HubZilla ecosystem would be a good deal
more active if only HubZilla was faster and easier to use.  

Users tend to be technical and highly motivated for decentralization.  

----------------------------------------------------------------------

### Developers

One, in the main. Mike MacGirvin.  
He has been involved in communication theory and infrastructure in his
career, and created Friendica and HubZilla.  
There is no other heavily contributing developers, although some others
have contributed here and there.  
Interview with Mike MacGirvin: https://medium.com/we-distribute/got-zot-mike-macgirvin-45287601ff19

----------------------------------------------------------------------

### Documentation state

Good.  
The specifications are clear and give enough detail to plan 
implementation.  

----------------------------------------------------------------------

### Separation of concerns

Zot6 was initiated in order to simplify the protocol, and to make its
functions better separated; Zot6 is still in a very early stage though.

----------------------------------------------------------------------

### Encryption

Good.  
The default encryption algorithm is considered a minimum, and
other/better algorithms can be negotiated between endpoints.  

----------------------------------------------------------------------

### Identity management

Built-in, which is the advantage of this protocol.  
> Zot is a JSON-based web framework for implementing secure decentralised communications and services. In order to provide this functionality, Zot creates a decentralised globally unique identifier for each hub on the network. This global identifier is not linked inextricably to DNS, providing the requisite mobility. Many existing decentralised communications frameworks provide the communication aspect, but do not provide remote access control and authentication. Additionally most of these are based on 'webfinger', which still binds identity to domain names and cannot support nomadic identity.  
Zot Protocol, Technical Introduction: https://project.hubzilla.org/help/en/developer/zot_protocol#Technical_Introduction

----------------------------------------------------------------------

### Project Archetype

Rocket Ship to Mars.  
We are aligned with them in our goals, but we cannot at present count
on a stable protocol.  
From the Technical Introduction:  
> Some aspects of well known "federation protocols" (webfinger, salmon, activitystreams, portablecontacts, etc.) may be used in zot, but we are not tied to them and will not be bound by them. Hubzilla project is attempting some rather novel developments in decentralised communications and if there is any need to diverge from such "standard protocols" we will do so without question or hesitation.

----------------------------------------------------------------------

## Scuttlebot

Scuttlebot is a distributed gossip protocol that Dominic Tarr has developed by exploring networks based on replication.  
Latest iteration is Secure Scuttlebot, and there is active interest from the community in improving the protocol and software.  

----------------------------------------------------------------------

### User base

Pretty varied but a seemingly high degree of technical proficiency.  
Interests like do-it-yourself, off-grid living, and urban farming are not unusual.  
Many seem to be idealistic with a look to cryptographic technologies and decentralization to solve problems.  
Also the community seems to be highly interconnected and sharing - which could be an 'early days' phenomenon but could also have something to do with the network's distribution method being based on social interconnections.  

----------------------------------------------------------------------

### Adoption

The number of users seem to be selfsustaining and slowly growing.  
Several applications have been written for secure scuttlebot by different people, though I (UW) think only Patchwork is widely used.  

----------------------------------------------------------------------

### Developers

2 high-input contributors, primary contributor being Dominic Tarr.

----------------------------------------------------------------------

### Documentation state

Somewhat scattered.  
Several people in the community recognizes this and there seem to be a project underway to rectify it.  

----------------------------------------------------------------------

### Separation of concerns

The basic concept of secure scuttlebot being a foundation on which other things are built and experimented with is good.  
That said.. the code base is probably messy, since aforementioned project wants to rewrite everything in Rust.  

----------------------------------------------------------------------

### Encryption

Hardcoded. Which is recognized as a problem and seen as a priority.  
There are people seeking grants which, if given, will be applied towards fixing it.  

----------------------------------------------------------------------

### Identity management

Built-in and based on public/private keys.  
In theory this *should* make it possible to export and import identities but I (UW) do not know if it is possible in practice.  

----------------------------------------------------------------------

### Project Archetype
Ad-hoc mix between Controlled Ecosystem and Wide Open.  
Development efforts seem to be somewhat anarchistic with little central control. Community is by and large in agreement with one another though.  

----------------------------------------------------------------------

## ActivityPub

ActivityPub is a specification for a protocol designed to allow many
different decentralized communications networks to interoperate.  

----------------------------------------------------------------------

### User base

Wide as diverse, as the ActivityPub protocol spans many networks.  
Mastodon in particular looms large.  
However, every network implements ActivityPub in its own way, and
there cannot be said to be 'one' user base; rather several.  

----------------------------------------------------------------------

### Adoption

Widespread adoption. Several networks uses ActivityPub and it seems like
more will be catching on.  

----------------------------------------------------------------------

### Developers

Many in total; particularly Mastodon seems to have a large pool of
contributors to draw from.  

----------------------------------------------------------------------

### Documentation state

Very good.  
Everything is well described and neatly compartmentalized.  
One criticism would be that oftentimes the specification of the protocol
assumes a good depth of knowledge on the part of the reader - which may
have been necessary to keep the specification to-the-point, but still,
it can be something of a mouthful.  

----------------------------------------------------------------------

### Separation of concerns

Excellent.  
Everything is separated and when there is a potential to split up
the concerns of the implementation, the specification makes this
possible, eg. group actors instead of server-implemented groups.  

----------------------------------------------------------------------

### Encryption

None specified.  
Left for the individual implementation to handle.  

----------------------------------------------------------------------

### Identity management

None specified.  
Left for the individual implementation to handle.  
Identity management is a huge gap. The fact that is hasn't been decided
upon means that many networks are now left only partly interoperable,
since each network decided for themselves how to go about it.  
However, it is not likely that every network would have followed any
given decision.  

----------------------------------------------------------------------

### Project Archetype

Wide Open.  
Specification is left broad on purpose. It is intended to sum up what
has been learned so far, about decentralized communications networks,
and guide future development efforts.  
However, the guide is not complete.  
Identity management in particular is left open-ended, and this has
probably been done on purpose.  
The field is new and the problem hard to solve. Presumably it was left
unspecified because no consensus was possible anyway; the field needs
experimentation.  

----------------------------------------------------------------------
