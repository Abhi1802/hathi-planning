
Requirements List for Internal Collaboration Network
=====================================================

```
Authour/Person to blame: UW

Document history: 
03 Dec 2018:
Creation
```

Scope
------

### In-scope

Hathi-ICN only needs to cover basic internal messaging functions.  
Hierarchically threaded discussions on arbitrary topics will do.  

- Recipient list
- Client->Server->Client Messages
- hierarchical display of original post + replies to original post

### Off-scope

- Encryption
- Tags and taxonomies
- Group client functionality
- Discoverability
- Migration
- Inter-server propagation

### Scenarios

1. Internal development
   - Discussion of Nier Automata  
     We should be able to cover all conceivable angles of gameplay
     without being confused 3 months hence.
   - Direct messages  
     So that we can organize The Resistance.

### Personas

1. Sandy Codex

   Sandy Codex is a coder. Or a manager. Or a database specialist. Or a researcher.  
   She wants to get things done, and she wants to get them done now, no hassles.  
   She is tired of web sites getting in the way of her work, she is tired of operating systems getting in the way of her work, and she is tired of her editor getting in the way of her work.  
   She will tend to like graphically low-key applications and efficient use of screen space.  
   She will also tend to like whatever features makes it less of a hassle to exchange opinions and information with other people, especially the ones she works with.  

1. Aunt Tillie

   Aunt Tillie is a woman of many interests. She didn't grow up with computers and aren't really that curious about them, but she does like her knitting, and being able to share her sought-after recipes over the internet.  
   She is also secretly a bit of a rebel, and a little curious about the political undercurrents of worldly affairs.  
  She prefers a system to be pretty intuitive, have practical uses such as recipe sharing, and if it provides a little bit more for her curiousity, so much the better.  
  She would be utterly lost if presented with install options involving memory- and resource management, but will generally regard developers (and their default options) to be trustworthy.  


Functionality
--------------


### Create a profile

A user should be able to create an account on a Hathi instance.  
The user should be expected to provide the following information:

* Some form of authentication credential for future use such as a username and password.
* A display name

The Hathi instance should store the salted hash of the password with the username and used to validate the user at a later time.  
The instance should also have a mechanism to verify the identity of the user creating the account. The following options are to be considered:

* Send an email with a confirmation code, to be entered by the user to the Hathi instance
* OAuth
* GPG key

Once the Hathi instance has verified the user's identity, the user will be able to login to the Hathi instance using the correct credentials; and the Hathi instance will provide other functionality available to an existing user account.

### Login to Hathi

A user should be able to enter the authentication credential provided when creating the account (see "Create an account" above) to login to their account on the Hathi instance.  
When the credential has been validated, the user is notified and the user is able to access functionality available to authenticated users.

### Update their login password

An authenticated user should be able to update their password.  
The user should provide their existing password and the new password.  
When the Hathi instance has validated that the provided existing password is correct it should replace the currently-stored salted hash with a salted hash of the new password.  

At this point, the user should be expected to use the new password.

### View an instance user's profile

Any user should be able to view another user's profile information.

### Sending a message

An authenticated user should be able to create and send a message.  
The user should be able to decide the note's recepient.

If any of the recipients are in another ActivityPub instance, the Hathi network should propagate the relevant messages to the recipients' instances for delivery there.

### Get a list of sent messages

An authenticated user should be able to retrieve a list of messages that they have sent.

### Edit a message

An authenticated user should be able to edit a message that they have previously created.
(But already-sent messages cannot be retroactively altered)

### Get a list of received messages

An authenticated user should be able to retrieve a list of messages they have received.  
These messages can be sent explicitly to them; or have been produced due to public posts created by a user they are following.

### View a message

A user should be able to view messages that they have been given access to by the message's creator.  
An authenticated user should also be able to view a message that they created on the Hathi instance.

If the message is in reply to another message, a reference to that message should be displayed.  

Similarly, if the message has replies, a reference to the replies should be displayed.

### Reply to a message

An authenticated user should be able to reply to a message that they are able to view.  
When this happens, the Hathi instance should include a reference to the ID of the message being replied to.

### View a list of messages in a hierarchical thread format

A user should be able to view the list of messages in a thread view.



User Interfaces
----------------

All interfaces will contain the following menu:

- Messages
  - Compose message
	- Send message
	- Cancel message
  - View message
    - Close message
  - Reply to message
    - Send message
    - Cancel message
- Recipient Lists
  - Add list
  - View list
    - Close list
  - Edit list
    - Add recipient
    - Remove recipient
    - Close list
  - Delete list


All interfaces will contain the following displays:

- Login
  By completion of which it is possible to access the remaining functionality.
- Menu  
  Lists a number of choices and does things when the choices are chosen.
- List  
  Lists a number of things and presents relevant things such as topics
  in a readable form.
  Also sender and date because we like to hold people accountable.
- Discussion view  
  A hierarchical, chronological presentation of messages.

### Web Interface
[Keith: Description]
