Requirements checklist
=======================

A comparison of our project to various other social platforms, in order
to ensure that we are not needlessly reinventing wheels.  

Requirements
-------------

What we are looking for is a productivity-oriented social network that
ensures privacy and is safe from unnecessary censorship or other
external influences.  
We are also looking to have the software providing the network free and
open source, since otherwise we are not guaranteed continued control
over what it does.
Lastly, as we want the network to be ubiquitous regardless of country of
origin or other circumstances, the network components should be low in
resource-consumption, a goal that is well aligned with our general
design preference of easily maintained focus on the task at hand - ie., 
no unnecessary distractions.

- Freedom of choice
  - Decentralized
  - Easy Migration
  - Extendability
  - Open Souce
- Stability
  - Handling of asynchronicity
  - Maintainable (lean, modular code base built on stable library modules)
  - Attack and censorship resistant
- Responsive
  - Task-focused (zero distractions)
  - Low resource consumption
- Secure
  - Protection of private data
  - End-to-end encryption

Platforms
----------

| Platform     | Groups   | Migration | Extendable | Asynchronous | Maintainable | Attack-res | Focused | Resourcelight | Private | Comments |
| :----------- | :------: | :-------: | :--------: | :----------: | :----------: | :--------: | :-----: | :-----------: | :-----: | :------: |
| Mastodon     | No       | Maybe     |            |              |              |            |         |               | ?       |          |
| Scuttlebot   | Channels | Yes       | Yes        | Yes          | ?            | ?          | Yes     | Yes           | Yes     | has channels |
| Diaspora     |          | ?         |            |              |              |            |         |               |         | did good funding, erred on delivery, not many is there now; facebook-oriented; might be expandable to use for productivity |
| Friendica    |          |           | Yes        |              | PHP/Site     |            |         |               |         |       |
| identica     |          |           |            |              |              |            |         |               |         | pretty good but mostly social; twitter-oriented |
| IRC          |          | No        | Yes        | No           | Yes          | No         | Yes     | Yes           | No      |       |
| Pleroma      |          |           | Maybe      | Yes          | Yes          |            |         | Yes           |         | Might be expandable to fulfill our needs |
| HubZilla     | Channels | Yes       | Yes        | Yes          | No           |            |         | No            | Yes     | Advanced, for both better and worse |
| GNU Social   |          |           |            |              |              |            |         |               |         | might be expandable to fulfill our needs |
| GangGo       |          |           |            |              |              |            |         |               |         | A Diaspora pod implemented in Golang? |
| MissKey      |          |           |            |              |              |            | No      |               | Yes     |       |
| Aardwolf     |          |           |            |              |              |            |         |               |         | Looks like they are in the startup phase. Somewhat similar goals as us. Maybe. |

Other Platforms
----------------
Facebook and Twitter is not listed on account of them being non-open source and centralized; they can be censored and are outside user control.  
Signal is not listed on account of a closed developer group; control is centralized and there is no public API (confirm?)  
